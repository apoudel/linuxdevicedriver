#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <stdint.h>
#include <sys/ioctl.h>

#include "i2c_arduino.h"

void change_display_text();
void clear_display_text();
void scroll_display_text();
void read_temperature();
void change_temperature_unit();

int main()
{
    if( access( DEVICE_FILE_PATH, F_OK ) == -1 )
    {
        printf("ERROR: Device driver not loaded !\n");
        exit(1);
    }

    int option = 0;
    char buff[ 100 ] = {0};
    do {
    	option = 0;

        printf("\n");
        printf("|============================================|\n");
        printf("|          Arduino Device Manager            |\n");
        printf("|============================================|\n");
        printf(" 1: Change display text.\n");
        printf(" 2: Clear display text.\n");
        printf(" 3: Start/Stop display text scroll.\n");
        printf(" 4: Change temperature unit.\n");
        printf(" 5: Read temperature.\n");
        printf(" 6: Quit.\n");
        printf("Option: ");
        gets( buff );
        sscanf(buff, "%d", &option);

        switch( option )
        {
            case 1:
            {
                change_display_text();
                break;
            }
            case 2:
            {
                clear_display_text();
                break;
            }
            case 3:
            {
                scroll_display_text();
                break;
            }
            case 4:
            {
                change_temperature_unit();
                break;
            }
            case 5:
            {
                read_temperature();
                break;
            }
            default:
                break;
        };

   } while( option != 6 );
}

void change_display_text()
{
    char text[ 1024 ] = {0};
    printf("Enter text to display: " );
    gets( text );

    int fd = open( DEVICE_FILE_PATH, O_WRONLY | O_SYNC );
    if( fd == -1 )
    {
        printf("\nERROR: Unable to open device file\n");
        return;
    }

    int len = strlen( text );

    if( len > 0 )
    {
        int wlen = write( fd, text, len );
        if( wlen == -1 )
            printf("ERROR: writing %d bytes to device failed. %s\n", len, strerror(errno));
    }
    close( fd );
}

void read_temperature()
{
    char text[ 20 ] = {0};

    int fd = open( DEVICE_FILE_PATH, O_RDONLY | O_SYNC );
    if( fd == -1 )
    {
        printf("\nERROR: Unable to open device file\n");
        return;
    }

    int result = read( fd, text, sizeof(text));
    if( result == -1 )
    {
        printf("ERROR: reading from device failed. %s\n", strerror(errno));
    }
    else
    {
        printf("Display text: %s\n", text);
    }

    close( fd );
}

void clear_display_text()
{
    int fd = open( DEVICE_FILE_PATH, O_WRONLY | O_SYNC );
    if( fd == -1 )
    {
        printf("\nERROR: Unable to open device file\n");
        return;
    }

    int result = ioctl( fd, IOCTL_CLEAR_DISPLAY, 5 );
    if( result != 0 )
        printf("ERROR: clearing display. %s\n", strerror(errno));

    close( fd );
}

void scroll_display_text()
{
    char option = 0;
    printf("Press 1 to start, 2 to stop: ");
    option = getchar();

    int fd = open( DEVICE_FILE_PATH, O_WRONLY | O_SYNC );
    if( fd == -1 )
    {
        printf("\nERROR: Unable to open device file\n");
        return;
    }

    if( option == '1' )
    {
        int result = ioctl( fd, IOCTL_SCROLL_DISPLAY, SCROLL_DISPLAY_START );
        if( result != 0 )
            printf("ERROR: start scrolling display. %s\n", strerror(errno));
    }
    else if( option = '2' )
    {
        int result = ioctl( fd, IOCTL_SCROLL_DISPLAY, SCROLL_DISPLAY_STOP );
        if( result != 0 )
            printf("ERROR: stop scrolling display. %s\n", strerror(errno));
    }

    close( fd );
}

void change_temperature_unit()
{
    char option = 0;
    printf("Press 'c' for Celcius, 'f' for Fahrenheit, 'k' for Kelvin: ");
    option = getchar();

    int fd = open( DEVICE_FILE_PATH, O_WRONLY | O_SYNC );
    if( fd == -1 )
    {
        printf("\nERROR: Unable to open device file\n");
        return;
    }

    if( option == 'c' )
    {
        int result = ioctl( fd, IOCTL_CHANGE_TEMP_UNIT, TEMP_UNIT_CELCIUS );
        if( result != 0 )
            printf("ERROR: changing temperature unit. %s\n", strerror(errno));
    }
    else if( option == 'k' )
    {
        int result = ioctl( fd, IOCTL_CHANGE_TEMP_UNIT, TEMP_UNIT_KELVIN );
        if( result != 0 )
            printf("ERROR: changing temperature unit. %s\n", strerror(errno));
    }
    else if( option == 'f' )
    {
        int result = ioctl( fd, IOCTL_CHANGE_TEMP_UNIT, TEMP_UNIT_FAHRENHEIT );
        if( result != 0 )
            printf("ERROR: changing temperature unit. %s\n", strerror(errno));
    }

    close( fd );
}
