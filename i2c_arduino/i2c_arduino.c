/**
 * @file   i2c_arduino.c
 * @author Aayush Poudel
 * @date   01/29/2018
 * @version 0.1
 * @brief   An character driver to talk to arduino to support the third article of my series on
 * Linux loadable kernel module development. This module maps to /dev/arduino_device
 * check http://www.aayushpoudel.info/category/linux-device-driver/ for a full description and follow-up descriptions.
 */
 
#include <linux/init.h>           // Macros used to mark up functions e.g. __init __exit
#include <linux/module.h>         // Core header for loading LKMs into the kernel
#include <linux/device.h>         // Header to support the kernel Driver Model
#include <linux/kernel.h>         // Contains types, macros, functions for the kernel
#include <linux/fs.h>             // Header for the Linux file system support
#include <linux/uaccess.h>        // Required for the copy to user function
#include <linux/cdev.h>           // For 'struct cdev'
#include <linux/i2c.h>            // For i2c related stuffs
#include <linux/delay.h>

#include "i2c_arduino.h"

#define  DEVICE_NAME "arduino_device" ///< The device will appear at /dev/arduino_device using this value
#define  CLASS_NAME  "arduino"        ///< The device class -- this is a character device driver

MODULE_LICENSE("GPL");            ///< The license type -- this affects available functionality
MODULE_AUTHOR("Aayush Poudel");   ///< The author -- visible when you use modinfo
MODULE_DESCRIPTION("A char driver to communicate to arduino");  ///< The description -- see modinfo
MODULE_VERSION("0.1");            ///< A version number to inform users

static char *dbg_tag = "i2c_arduino";
static unsigned short	i2c_address = 0x08;

module_param(dbg_tag, charp, S_IRUGO);
module_param(i2c_address, ushort, S_IRUGO);

MODULE_PARM_DESC(dbg_tag, "The name to display in /var/log/kern.log");

struct arduino_device_t
{
    dev_t           dev;            ///< To store major/minor number
    struct cdev     cdev;           ///< To store kernel char device handle
    struct device   *device;        ///< The device-driver class struct pointer
    struct class    *class;         ///< The device-driver device struct pointer

    struct i2c_client  *i2c_client;
    struct i2c_adapter *i2c_adapter;

    char display_text[ DISPLAY_TEXT_MAX_LEN + 1 ];   ///< Memory for the string that is passed from userspace
    int display_text_len;            ///< Used to remember the size of the string stored

    char temperature_text[ TEMPERATURE_TEXT_MAX_LEN + 1 ];
};

static struct arduino_device_t    arduino_device;

// The prototype functions for the character driver -- must come before the struct definition
static int     dev_open(struct inode *, struct file *);
static int     dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);
static long    dev_ioctl(struct file*, unsigned int, unsigned long);

/** @brief Devices are represented as file structure in the kernel. The file_operations structure from
 *  /linux/fs.h lists the callback functions that you wish to associated with your file operations
 *  using a C99 syntax structure. char devices usually implement open, read, write and release calls
 */
static struct file_operations arduino_device_fops =
{
   .open = dev_open,
   .read = dev_read,
   .write = dev_write,
   .unlocked_ioctl = dev_ioctl,
   .release = dev_release,
};

/** @brief The module initialization function
 *  @return returns 0 if successful
 */
static int __init arduino_device_init(void){
    int result = 0;
    printk(KERN_INFO "%s: Initializing\n", dbg_tag);

    memset( &arduino_device, 0, sizeof(struct arduino_device_t));

    arduino_device.i2c_adapter = i2c_get_adapter(1);

    if( IS_ERR( arduino_device.i2c_adapter ))
    {
        printk( KERN_ALERT "%s: i2c_get_adapter failed\n", dbg_tag);
        return PTR_ERR( arduino_device.i2c_adapter );        // Correct way to return an error on a pointer
    }

    arduino_device.i2c_client = i2c_new_dummy( arduino_device.i2c_adapter, i2c_address);

    if( IS_ERR( arduino_device.i2c_client ))
    {
        printk( KERN_ALERT "%s: Error registering i2c device\n", dbg_tag);
        return PTR_ERR( arduino_device.i2c_client );
    }

    // Allocate major/minor number for 1 device
    result = alloc_chrdev_region( &arduino_device.dev, 0, 1, "arduino_device");
    if( result != 0 )
    {
        printk( KERN_ALERT "%s: Failed to allocate char device\n", dbg_tag);
        return result;
    }

    printk( KERN_INFO "%s: device allocted correctly with major number %d\n", dbg_tag, MAJOR(arduino_device.dev));

    cdev_init( &arduino_device.cdev, &arduino_device_fops);
    arduino_device.cdev.owner = THIS_MODULE;

    result = cdev_add( &arduino_device.cdev, arduino_device.dev, 1);
    if( result != 0 )
    { 
        printk( KERN_ALERT "%s: cdev_add failed\n", dbg_tag);
        return result;
    }

    // Register the device class
    arduino_device.class = class_create( THIS_MODULE, CLASS_NAME );
    if( IS_ERR( arduino_device.class ))        // Check for error and clean up if there is
    {
        cdev_del( &arduino_device.cdev );
        unregister_chrdev_region( arduino_device.dev, 1);

        printk( KERN_ALERT "%s: Failed to register device class\n", dbg_tag);
        return PTR_ERR( arduino_device.class );        // Correct way to return an error on a pointer
    }
    printk( KERN_INFO "%s: device class registered correctly\n", dbg_tag);

    // Create device file /dev/arduino_device
    arduino_device.device = device_create( arduino_device.class, NULL, arduino_device.dev, NULL, DEVICE_NAME);
    if( IS_ERR( arduino_device.device ))                   // Clean up if there is an error
    {
        cdev_del( &arduino_device.cdev );
        class_destroy( arduino_device.class );               // Repeated code but the alternative is goto statements
        unregister_chrdev_region( arduino_device.dev, 1);

        printk( KERN_ALERT "%s: Failed to create the device\n", dbg_tag);
        return PTR_ERR( arduino_device.device );
    }
    printk( KERN_INFO "%s: device class created correctly\n", dbg_tag); // Made it! device was initialized
    return 0;

}

/** @brief The module cleanup function
 */
static void __exit arduino_device_exit(void){
    device_destroy( arduino_device.class, arduino_device.dev );    // remove the device

    class_unregister( arduino_device.class );                   // unregister the device class
    class_destroy( arduino_device.class );                      // remove the device class

    cdev_del( &arduino_device.cdev );
    unregister_chrdev_region( arduino_device.dev, 1);

    i2c_unregister_device( arduino_device.i2c_client );
    printk(KERN_INFO "%s: Goodbye\n", dbg_tag);
}

/** @brief The device open function that is called each time the device is opened
 *  This will only increment the number_opens counter in this case.
 *  @param inodep A pointer to an inode object (defined in linux/fs.h)
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 */
static int dev_open(struct inode *inodep, struct file *filep){
   printk( KERN_INFO "%s: Device successfully opened\n", dbg_tag);
   return 0;
}

/** @brief This function is called whenever device is being read from user space i.e. data is
 *  being sent from the device to the user. In this case is uses the copy_to_user() function to
 *  send the buffer string to the user and captures any errors.
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 *  @param buffer The pointer to the buffer to which this function writes the data
 *  @param len The length of the b
 *  @param offset The offset if required
 */
static ssize_t dev_read( struct file *filep, char *buffer, size_t len_ask, loff_t *offset )
{
    int result = 0, i = 0;

    result = i2c_smbus_write_byte( arduino_device.i2c_client, I2C_CMD_TEMP_READ );
    if( result != 0 )
    {
        printk( KERN_INFO "%s: Failed to send I2C_CMD_TEMP_READ command to device\n", dbg_tag );
        return result;
    }

    msleep(20);
    do {
        result = i2c_smbus_read_byte( arduino_device.i2c_client );
        if( result < 0 )
        {
            printk( KERN_INFO "%s: Failed to read byte from device, result %d\n", dbg_tag, result );
            return result;
        }
        msleep(20);
        arduino_device.temperature_text[ i++ ] = (char) result;

    } while( arduino_device.temperature_text[ i-1 ] != '\0' );

    i = strlen( arduino_device.temperature_text );
    if( len_ask <= i )
    {
        result = copy_to_user( buffer, arduino_device.temperature_text, len_ask );
        i = len_ask;
    }
    else
        result = copy_to_user( buffer, arduino_device.temperature_text, i );

    if( result == 0 )
    {
        printk( KERN_INFO "%s: Sent %d bytess to the user\n", dbg_tag, i );
        return i;    // return number of byte copied to used;
    }
    else {
        printk( KERN_INFO "%s: Failed to send %d characters to the user\n", dbg_tag, i );
        return -EFAULT;              // Failed -- return a bad address message (i.e. -14)
    }
}

/** @brief This function is called whenever the device is being written to from user space i.e.
 *  data is sent to the device from the user. The data is copied to the message[] array
 *  @param filep A pointer to a file object
 *  @param buffer The buffer to that contains the string to write to the device
 *  @param len The length of the array of data that is being passed in the const char buffer
 *  @param offset The offset if required
 */
static ssize_t dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset)
{
    int result = 0, byte_written = -EREMOTEIO;
    int data_index = 0, data_chunk = 0, data_remaining = 0;

    printk( KERN_INFO "%s: Received %zu letters from the user\n", dbg_tag, len);

    if( len <= DISPLAY_TEXT_MAX_LEN )
    {
        result = copy_from_user( arduino_device.display_text, buffer, len );

        if( result == 0 )
        {
            data_remaining = len;

            // If message is greater than I2C_DATA_CHUNK_SIZE bytes send it in chunks of I2C_DATA_CHUNK_SIZE byte
            for( data_chunk = 0; data_remaining > 0; data_chunk++ )
            {
                unsigned char i2c_command = 0;
                unsigned char i2c_data_length = 0;

                if( data_chunk == 0 )
                    i2c_command = I2C_CMD_DATA;
                else
                {
                    i2c_command = I2C_CMD_DATA_CHUNK;

                    // wait before sending remaining chunk of data;
                    msleep( 200 );
                }

                data_index += ( data_chunk * I2C_DATA_CHUNK_SIZE );

                if( len >= (I2C_DATA_CHUNK_SIZE * ( data_chunk + 1)))
                {
                    i2c_data_length = I2C_DATA_CHUNK_SIZE;
                    data_remaining -= I2C_DATA_CHUNK_SIZE;
                }
                else
                {
                     i2c_data_length = len - (I2C_DATA_CHUNK_SIZE * data_chunk);
                     data_remaining -= i2c_data_length;
                }

                printk( KERN_INFO "%s: i2c_command %d, i2c_data_length %d data_remaining %d, data_index %d", dbg_tag, i2c_command, i2c_data_length, data_remaining, data_index);

                result = i2c_smbus_write_block_data( arduino_device.i2c_client, i2c_command, i2c_data_length, &arduino_device.display_text[ data_index ]);
                if( result != 0 )
                {
                    printk( KERN_ALERT "%s: i2c_smbus_write_block_data failed", dbg_tag);
                    byte_written = result;
                    break;
                }
            }

	        if( result == 0 )
            {
                // Need to return number to byte written
                byte_written =  len;
                arduino_device.display_text_len = len;  // store the length of the stored message
            }
        }
        else
        {
            printk( KERN_ALERT "%s: copy_from_user failed", dbg_tag);
        }
    }

    return byte_written;
}

/** @brief The device release function that is called whenever the device is closed/released by
 *  the userspace program
 *  @param inodep A pointer to an inode object (defined in linux/fs.h)
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 */
static int dev_release(struct inode *inodep, struct file *filep){
   printk( KERN_INFO "%s: Device successfully closed\n", dbg_tag);
   return 0;
}

/**
 *
 *
 */
static long dev_ioctl(struct file *filep, unsigned int ioctl_num, unsigned long ioctl_param )
{
    int result = 0;
    uint8_t value = ioctl_param;

    switch( ioctl_num )
    {
        case IOCTL_CLEAR_DISPLAY: 

            result = i2c_smbus_write_byte( arduino_device.i2c_client, I2C_CMD_CLEAR_DISPLAY );
            printk( KERN_INFO "%s: IOCTL_CLEAR_DISPLAY result %d\n", dbg_tag, result);

            break;

        case IOCTL_SCROLL_DISPLAY:

            result = i2c_smbus_write_byte_data( arduino_device.i2c_client, I2C_CMD_SCROLL_DISPLAY, value );
            printk( KERN_INFO "%s: IOCTL_SCROLL_DISPLAY value %d, result %d\n", dbg_tag, value, result);

            break;

        case IOCTL_CHANGE_TEMP_UNIT:

            result = i2c_smbus_write_byte_data( arduino_device.i2c_client, I2C_CMD_TEMP_UNIT, value );
            printk( KERN_INFO "%s: IOCTL_TEMP_UNIT value %d, result %d\n", dbg_tag, value, result);
            break;

        default:
            printk( KERN_INFO "%s: invalid ioctl_num %d\n", dbg_tag, ioctl_num);
            break;
    };

    return result;
}

/** @brief A module must use the module_init() module_exit() macros from linux/init.h, which
 *  identify the initialization function at insertion time and the cleanup function (as
 *  listed above)
 */
module_init( arduino_device_init );
module_exit( arduino_device_exit );

