#include <Wire.h>
#include <LiquidCrystal.h>

#include "i2c_arduino.h"

const int rs = 34, en = 35, d4 = 24, d5 = 25, d6 = 26, d7 = 27;
LiquidCrystal lcd( rs, en, d4, d5, d6, d7 );

bool scroll_display = false;

// Display text length should be greater than 16
String display_text;

// Index from where to read display_text for scrolling
int display_text_scroll_index = 0;

// Default temperature unit Kelvin
char temperature_display_unit = 'f';
char temperature_display_text[ 17 ] = {0};

bool temperature_read = false;

void receiveEvent( int howMany )
{
    int was_scrolling = false, command = Wire.read();

    static int display_text_insert_index = 0;

    switch( command )
    {
        case I2C_CMD_DATA:
        {
            if( scroll_display )
            {
                was_scrolling = true;
                scroll_display = false;
            }
            display_text = "";
            display_text.reserve( DISPLAY_TEXT_MAX_LEN );
            display_text_scroll_index = 0;

            // Start saving data from index 0
            display_text_insert_index = 0;

            int i = 0, data_len = Wire.read();
            
            for(; i < data_len; ++i, ++display_text_insert_index)
            {
                char data = Wire.read();
                if( isPrintable( data ))
                {
                    display_text += data;
                }
            }

            lcd.setCursor(0, 0);
            lcd.print( display_text );

            if( was_scrolling )
                scroll_display = true;

            break;
        }
        case I2C_CMD_DATA_CHUNK:
        {
            if( scroll_display )
            {
                was_scrolling = true;
                scroll_display = false;
            }

            int i = 0, data_len = Wire.read();
            for(; i < data_len; ++i, ++display_text_insert_index)
            {
                char data = Wire.read();
                if( isPrintable( data ))
                {
                    display_text += data;
                }
            }

            if( was_scrolling )
                scroll_display = true;

            break;
        }
        case I2C_CMD_TEMP_UNIT:
        {
            temperature_display_unit = Wire.read();
            break;
        }
        case I2C_CMD_CLEAR_DISPLAY:
        {
            scroll_display = false;
            display_text = "";
            display_text_scroll_index = 0;
            lcd.clear();
            break;
        }
        case I2C_CMD_SCROLL_DISPLAY:
        {
            int data = Wire.read();
            if( data )
            {
                scroll_display = true;
            }
            else
            {
                scroll_display = false;
            }
            break;
        }
        case I2C_CMD_TEMP_READ:
        {
            temperature_read = true;
            break;
        }
        default:
        {
            Serial.print("Unknown command: ");
            Serial.println(command, HEX);

            break;
        }
    };

    // Discard all data
    while( Wire.available() )
    {
        int data = Wire.read();        
        Serial.print("Discarding data: ");
        Serial.println(data, HEX);
    }
}

void requestEvent()
{
    static int temperature_display_text_index = 0;

    if( temperature_read )
    {
        Wire.write( temperature_display_text[ temperature_display_text_index++ ]);

        if( temperature_display_text[ temperature_display_text_index - 1 ] == '\0' )
        {
            temperature_read = false;
            temperature_display_text_index = 0;
        }
    }
}

/*
 * Return substring of display_text to display in LCD for scrolling effect
 */
String get_display_text()
{
    String tmp_text = "";
    
    if( display_text_scroll_index > display_text.length() )
        display_text_scroll_index = 0;

    int display_length = display_text.length() - display_text_scroll_index;
    if( display_length >= 16 )
    {
        tmp_text = display_text.substring( display_text_scroll_index, display_text_scroll_index + 16 ); 
    }
    else if( display_length <= 14 )
    {
        int i = 16 - display_length - 1;
        tmp_text = display_text.substring( display_text_scroll_index ) + " " + display_text.substring( 0, i ) ;
    }
    else
    {
        tmp_text = display_text.substring( display_text_scroll_index ) + " ";
    }

    display_text_scroll_index++;
    return tmp_text;
}

/** Converts input from a thermistor voltage divider to a temperature value.
 * The voltage divider consists of thermistor Rt and series resistor R0.
 * The value of R0 is equal to the thermistor resistance at T0.
 */
float read_temperature( float *tc, float *tf )
{
    const int analog_pin = 0; // replace 0 with analog pin
    const float inv_beta = 1.00 / 4050.00;   // replace "Beta" with beta of thermistor

    const  float adc_max = 1023.00;
    const float inv_T0 = 1.00 / 298.15;   // room temp in Kelvin

    float  K, F, C, adc_val = analogRead( analog_pin );

    K = 1.00 / (inv_T0 + inv_beta*(log ( adc_max / adc_val - 1.00)));
    C = K - 273.15;                     // convert to Celsius
    F = ((9.0*C)/5.00) + 32.00;     // convert to Fahrenheit

    if( tc != NULL )
        *tc = C;

    if( tf != NULL )
        *tf = F;

    return K;
}

/* Get temperature text
 *
 */
void get_temperature_text( char *buff, int len)
{
    char temp_str[6] = {0};

    float temp_k = 0, temp_c = 0, temp_f = 0;

    temp_k = read_temperature( &temp_c, &temp_f);

    if( temperature_display_unit == TEMP_UNIT_KELVIN )
    {
        dtostrf( temp_k, 3, 2, temp_str );
        snprintf( buff, len, "Temp %s K  ", temp_str );
    }
    else if( temperature_display_unit == TEMP_UNIT_CELCIUS )
    {
        dtostrf( temp_c, 3, 2, temp_str );
        snprintf( buff, len, "Temp %s C  ", temp_str );
    }
    else if( temperature_display_unit == TEMP_UNIT_FAHRENHEIT )
    {
        dtostrf( temp_f, 3, 2, temp_str );
        snprintf( buff, len, "Temp %s F  ", temp_str );
    }
}

void setup()
{
    Serial.begin(9600);
    Wire.begin( I2C_DEVICE_ADDRESS );
    Wire.onReceive( receiveEvent );
    Wire.onRequest( requestEvent );

    analogReference( DEFAULT );

    pinMode( LED_BUILTIN, OUTPUT );

    display_text = "Aayush Maya Anna";

    // set up the LCD's number of columns and rows:
    lcd.begin(16, 2);
    lcd.print( display_text );
}

void loop()
{
    if( scroll_display )
    {
        lcd.setCursor(0, 0);
        lcd.print( get_display_text() );
    }

    get_temperature_text( temperature_display_text, 17 );

    lcd.setCursor( 0,1 );
    lcd.print( temperature_display_text );

    delay(1000);
    
    if( digitalRead( LED_BUILTIN ) )
    {
        digitalWrite( LED_BUILTIN, LOW );
    }
    else
    {
        digitalWrite( LED_BUILTIN, HIGH );
    }
}
