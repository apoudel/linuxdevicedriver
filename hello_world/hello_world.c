#include <linux/init.h>             // Macros used to mark up functions e.g., __init __exit
#include <linux/module.h>           // Core header for loading modules into the kernel
#include <linux/kernel.h>           // Contains types, macros, functions for the kernel

MODULE_LICENSE("GPL");              ///< The license type -- this affects runtime behavior
MODULE_AUTHOR("Aayush Poudel");     ///< The author -- visible when you use modinfo
MODULE_DESCRIPTION("A hello world linux driver.");  ///< The description -- see modinfo
MODULE_VERSION("0.1");              ///< The version of the module

static char *name = "hello_world";  ///< An example module argument -- default value is "hello_world"
module_param(name, charp, S_IRUGO); ///< Param desc. charp = char ptr, S_IRUGO can be read/not changed
MODULE_PARM_DESC(name, "The name to display in /var/log/kern.log");  ///< parameter description

/** @brief The module initialization function
 * The static keyword restricts the visibility of the function to within this C file. The __init/__exit
 * macro tells the compiler to put the variable or the function in a special section. This function
 * is only used at initialization time and that it can be discarded and its memory freed up after that point.
 * @return returns 0 if successful
 **/
static int __init hello_world_init(void){
   printk(KERN_INFO "%s: Hello from the other side\n", name);
   return 0;
}

/** @brief The module cleanup function
 **/
static void __exit hello_world_exit(void){
   printk(KERN_INFO "%s: Goodbye\n", name);
}

/** @brief A module must use the module_init() module_exit() macros from linux/init.h, which
 * identify the initialization function at insertion time and the cleanup function (as
 * listed above)
 **/
module_init( hello_world_init );
module_exit( hello_world_exit );

