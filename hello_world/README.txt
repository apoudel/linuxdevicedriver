 How to build
=========================
 $ make

 load/unload module
=========================
 $ sudo insmode hello_world.ko name=my_hello_world
 $ sudo rmmod hello_world.ko

 Checking logs
=========================
 tail -f /var/log/kern.log

 Checking module info
=========================
 cat /sys/module/hello_world/parameters/name
