/**
 * @file   char_device.c
 * @author Aayush Poudel
 * @date   01/19/2018
 * @version 0.1
 * @brief   An introductory character driver to support the second article of my series on
 * Linux loadable kernel module development. This module maps to /dev/axp_device and
 * check http://www.aayushpoudel.info/category/linux-device-driver/ for a full description and follow-up descriptions.
 */
 
#include <linux/init.h>           // Macros used to mark up functions e.g. __init __exit
#include <linux/module.h>         // Core header for loading LKMs into the kernel
#include <linux/device.h>         // Header to support the kernel Driver Model
#include <linux/kernel.h>         // Contains types, macros, functions for the kernel
#include <linux/fs.h>             // Header for the Linux file system support
#include <linux/uaccess.h>        // Required for the copy to user function
#include <linux/cdev.h>           // For 'struct cdev'

#define  DEVICE_NAME "axp_device" ///< The device will appear at /dev/axp_device using this value
#define  CLASS_NAME  "axp"        ///< The device class -- this is a character device driver

MODULE_LICENSE("GPL");            ///< The license type -- this affects available functionality
MODULE_AUTHOR("Aayush Poudel");   ///< The author -- visible when you use modinfo
MODULE_DESCRIPTION("A simple Linux char driver");  ///< The description -- see modinfo
MODULE_VERSION("0.1");            ///< A version number to inform users

static char *dbg_tag = "char_device";
module_param(dbg_tag, charp, S_IRUGO);
MODULE_PARM_DESC(dbg_tag, "The name to display in /var/log/kern.log");

# define MESSAGE_SIZE 512

struct char_device_t
{
    dev_t           dev;            ///< To store major/minor number
    struct cdev     cdev;           ///< To store kernel char device handle
    struct device   *device;        ///< The device-driver class struct pointer
    struct class    *class;         ///< The device-driver device struct pointer
    char message[ MESSAGE_SIZE ];   ///< Memory for the string that is passed from userspace
    int size_of_message;            ///< Used to remember the size of the string stored
    int number_opens;               ///< Counts the number of times the device is opened
};

static struct char_device_t    char_device;

// The prototype functions for the character driver -- must come before the struct definition
static int     dev_open(struct inode *, struct file *);
static int     dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);

/** @brief Devices are represented as file structure in the kernel. The file_operations structure from
 *  /linux/fs.h lists the callback functions that you wish to associated with your file operations
 *  using a C99 syntax structure. char devices usually implement open, read, write and release calls
 */
static struct file_operations char_device_fops =
{
   .open = dev_open,
   .read = dev_read,
   .write = dev_write,
   .release = dev_release,
};

/** @brief The module initialization function
 *  @return returns 0 if successful
 */
static int __init char_device_init(void){
    int result = 0;
    printk(KERN_INFO "%s: Initializing\n", dbg_tag);

    memset( &char_device, 0, sizeof(struct char_device_t));

    // Allocate major/minor number for 1 device
    result = alloc_chrdev_region( &char_device.dev, 0, 1, "char_device");
    if( result != 0 )
    {
        printk( KERN_ALERT "%s: Failed to allocate char device\n", dbg_tag);
        return result;
    }

    printk( KERN_INFO "%s: device allocted correctly with major number %d\n", dbg_tag, MAJOR(char_device.dev));

    cdev_init( &char_device.cdev, &char_device_fops);
    char_device.cdev.owner = THIS_MODULE;

    result = cdev_add( &char_device.cdev, char_device.dev, 1);
    if( result != 0 )
    { 
        printk( KERN_ALERT "%s: cdev_add failed\n", dbg_tag);
        return result;
    }

    // Register the device class
    char_device.class = class_create( THIS_MODULE, CLASS_NAME );
    if( IS_ERR( char_device.class ))        // Check for error and clean up if there is
    {
        cdev_del( &char_device.cdev );
        unregister_chrdev_region( char_device.dev, 1);

        printk( KERN_ALERT "%s: Failed to register device class\n", dbg_tag);
        return PTR_ERR( char_device.class );        // Correct way to return an error on a pointer
    }
    printk( KERN_INFO "%s: device class registered correctly\n", dbg_tag);

    // Create device file /dev/axp_device
    char_device.device = device_create( char_device.class, NULL, char_device.dev, NULL, DEVICE_NAME);
    if( IS_ERR( char_device.device ))                   // Clean up if there is an error
    {
        cdev_del( &char_device.cdev );
        class_destroy( char_device.class );               // Repeated code but the alternative is goto statements
        unregister_chrdev_region( char_device.dev, 1);

        printk( KERN_ALERT "%s: Failed to create the device\n", dbg_tag);
        return PTR_ERR( char_device.device );
    }
    printk( KERN_INFO "%s: device class created correctly\n", dbg_tag); // Made it! device was initialized
    return 0;
}

/** @brief The module cleanup function
 */
static void __exit char_device_exit(void){
    device_destroy( char_device.class, char_device.dev );    // remove the device

    class_unregister( char_device.class );                   // unregister the device class
    class_destroy( char_device.class );                      // remove the device class

    cdev_del( &char_device.cdev );
    unregister_chrdev_region( char_device.dev, 1);

    printk(KERN_INFO "%s: Goodbye\n", dbg_tag);
}

/** @brief The device open function that is called each time the device is opened
 *  This will only increment the number_opens counter in this case.
 *  @param inodep A pointer to an inode object (defined in linux/fs.h)
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 */
static int dev_open(struct inode *inodep, struct file *filep){
   char_device.number_opens++;
   printk(KERN_INFO "%s: Device has been opened %d time(s)\n", dbg_tag, char_device.number_opens);
   return 0;
}

/** @brief This function is called whenever device is being read from user space i.e. data is
 *  being sent from the device to the user. In this case is uses the copy_to_user() function to
 *  send the buffer string to the user and captures any errors.
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 *  @param buffer The pointer to the buffer to which this function writes the data
 *  @param len The length of the b
 *  @param offset The offset if required
 */
static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset)
{
    int error_count = 0;
    // copy_to_user has the format ( * to, *from, size) and returns 0 on success
    error_count = copy_to_user(buffer, char_device.message, char_device.size_of_message);
 
    if (error_count==0)
    {
        printk( KERN_INFO "%s: Sent %d characters to the user\n", dbg_tag, char_device.size_of_message);

        error_count = char_device.size_of_message;
        char_device.size_of_message = 0;
        return error_count;
   }
   else {
       printk( KERN_INFO "%s: Failed to send %d characters to the user\n", dbg_tag, error_count);
       return -EFAULT;              // Failed -- return a bad address message (i.e. -14)
   }
}
 
/** @brief This function is called whenever the device is being written to from user space i.e.
 *  data is sent to the device from the user. The data is copied to the message[] array
 *  @param filep A pointer to a file object
 *  @param buffer The buffer to that contains the string to write to the device
 *  @param len The length of the array of data that is being passed in the const char buffer
 *  @param offset The offset if required
 */
static ssize_t dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset)
{
    int error_count = 0;
    if( len <= MESSAGE_SIZE )
    {
        error_count = copy_from_user(char_device.message, buffer, len);
        if( error_count == 0 )
        {
            char_device.size_of_message = len;  // store the length of the stored message

            printk( KERN_INFO "%s: Received %zu letters from the user\n", dbg_tag, len);
            return len;
        }
    }

    return 0;
}

/** @brief The device release function that is called whenever the device is closed/released by
 *  the userspace program
 *  @param inodep A pointer to an inode object (defined in linux/fs.h)
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 */
static int dev_release(struct inode *inodep, struct file *filep){
   printk( KERN_INFO "%s: Device successfully closed\n", dbg_tag);
   return 0;
}

/** @brief A module must use the module_init() module_exit() macros from linux/init.h, which
 *  identify the initialization function at insertion time and the cleanup function (as
 *  listed above)
 */
module_init( char_device_init );
module_exit( char_device_exit );

