 How to build
=========================
 $ make

 load/unload module
=========================
 $ sudo insmode char_device.ko dbg_tag=my_char
 $ sudo rmmod char_device.ko

 Checking logs
=========================
 tail -f /var/log/kern.log

 Checking module info
=========================
 cat /sys/module/char_device/parameters/dbg_tag

 Test
==========================

 $ sudo python
 >>> f = open("/dev/axp_device", 'w')
 >>> f.write("Hello World")
 >>> f.close()

 >>> f = open("/dev/axp_device", 'r')
 >>> f.read()
 'Hello World'
 >>> f.close()

 udev rules
============================

